#!/usr/bin/perl

use strict;
use warnings;

my $start = 0;
my $delim = "<xxx7>";
my $count = 0;
LINE: while (my $line = <STDIN>)
{
    if ($start)
    {
        $line =~ s/^\s*(.*?)\s*$/$1/;
        next if $line =~ /^<\/?([bp]|div)/;
        next if $line =~ /<!--/;
        next if $line =~ /-->/;
        last LINE if ($line =~ m/$delim/);
        next if $line =~ /^<\/?\w*>$/;
        my $out = $line
            =~ s/\-\.//gr # weird -. два раза встречается
            =~ s/\&nbsp;/ /gr
            =~ s/([,;:.!?"'()])/ \1 /gr
            =~ s/^<i>/<dd> /r   # Эпиграф
            =~ s/<i>//gr        # начало ударения
            =~ s/<\/i>//gr      # конец ударения
            =~ s/^[ ]+//gr =~ s/[ ]+$//r # strip
            =~ s/ +/ /gr
            =~ s/ \-\-/ —/gr # тире
            =~ s/([абвгдежзиклмнопрстуфхцчшыюя])\-\- /$1/gr # переводы слов
            =~ s/\-{5}//r # разделитель?
            =~ s/ \-/ — /gr # остатки неправильных тире и дефисов
            # =~ s/ — / /gr
            ;
        next if $out eq "";
        if ($out =~ /^<dd>$/)
        {
            $count++;
        } else
        {
            $out =~ s/^<dd> +//; # строки ↔ параграфам
            print STDOUT "$out\n";
        }
    } elsif ($line =~ m/$delim/)
    {
        $start = 1;
    }
}


print STDERR "Empty lines count : $count\n";

