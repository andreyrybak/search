# Информационный поиск. ИТМО 2014


Для сборки нужен `ghc` и `cabal`.

Пакеты apt-get:

* ghc
* cabal-install

Используемые библиотеки (пакеты для cabal):

* ansi-terminal
* split
* containers

Команды для установки:

    $ sudo apt-get update && sudo apt-get install ghc cabal-install
    $ cabal update && cabal install ansi-terminal split containers

Файлы:

* `prepare.sh` и `prepare.pl` — предподготовка текста из [оригинального
  html-файла](http://az.lib.ru/t/tolstoj_lew_nikolaewich/text_0080.shtml)
* `makeFirst.sh` и `makeSecond.sh` — скрипты сборки

## Лабораторная работа № 1

Сборка и запуск:

    $ ./first.sh

Результат выполнения в файле [`resultFirst.txt`](raw/master/resultFirst.txt).

## Лабораторная работа № 2

Сборка и запуск:

    $ ./second.sh

### Формат запроса

Запросы вводятся в [ДНФ](https://ru.wikipedia.org/wiki/%D0%94%D0%9D%D0%A4).

Логические операторы (использовать можно любой из вариантов):

* `AND, and, &&, &, И `
* `OR, or, |, ||, ИЛИ `
* `NOT, not`

Примеры запросов:

* Анна И Степан
* позволил И себе
* Анна И приедет
* дура

Первый запрос проходит дольше, так как обратный индекс строится лениво.

### Команды:

* `:quit`, `:q` — выход
* `:n`, `:p` — перейти к следующему/предыдущему результату поиска
