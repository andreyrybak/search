#!/bin/bash

readonly ORIG="anna-original.html"
readonly UTF8="anna-utf8.html"
readonly PREP="anna-prepared.html"

# download file
if [[ ! -f "$ORIG" ]];
then
    wget http://az.lib.ru/t/tolstoj_lew_nikolaewich/text_0080.shtml -O $ORIG
fi

readonly ORIG_ENCODING=CP1251

if [[ ! -f "$UTF8" ]];
then
    iconv -f "$ORIG_ENCODING" $ORIG -o $UTF8
fi

if [[ ! -f "$PREP" ]];
then
    perl prepare.pl < $UTF8 > $PREP
fi


