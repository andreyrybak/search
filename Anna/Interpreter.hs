{-# LANGUAGE DeriveDataTypeable #-}
module Anna.Interpreter
       where

import System.Exit
import System.IO
import Prelude hiding (Word)

import Control.Monad
import Data.List
import qualified Data.List.Split as LS
import qualified Data.IntSet as S
import Data.Bool()
import Data.Monoid (Monoid, mconcat)
import Data.Maybe()

import System.Console.ANSI

import IIndex

data SimpleTerm = STerm String | NotTerm String
instance Show SimpleTerm where
  show (STerm w) = "STerm " ++ w
  show (NotTerm w) = "NotTerm " ++ w

type SimpleQuery = [[SimpleTerm]] -- query in form of DNF

nots, ors, ands :: [String]
nots = ["NOT", "not"]
ors = ["|", "||", "OR", "or", "ИЛИ"]
ands = ["&", "&&", "AND", "and", "И"]

mysplit :: Eq a => [a] -> [a] -> [[a]]
mysplit delims line = LS.splitWhen (flip elem delims) line

makeSimpleQuery :: String -> Maybe SimpleQuery
makeSimpleQuery s = if test ws
    then Just $ map (map f) ts
    else Nothing
    where
    ws = words s
    cs = mysplit ors ws -- LS.splitOn ors ws
    ts = map (mysplit ands) cs
    f x = case x of
        [_, w] -> NotTerm w
        [w]      -> STerm w
        _        -> error "not supposed to happen"
    test [_] = True
    test q = all (`elem` (ands ++ ors)) ops
       where ops = evens $ filterQuery q
evens :: [a] -> [a]
evens = odds . tail
          
runSimpleQuery :: IIndex -> SimpleQuery -> S.IntSet
runSimpleQuery _ [] = S.empty
runSimpleQuery i q  = foldl1 S.union (map (runAndQuery i) q)

-- lookUp queries of type : w1 AND w2 AND NOT w3
runAndQuery :: IIndex -> [SimpleTerm] -> S.IntSet
runAndQuery i@(IIndex d _) cs = r (divideQuery cs) where
  r (pos, neg) = (
      if (null pos)
      then (S.fromList (enumFromTo 0 ((length d) - 1)))
      else (lookUpAll i (map fromSimpleTerm pos))
    ) S.\\ (lookUpAny i (map fromSimpleTerm neg)) where
    fromSimpleTerm :: SimpleTerm -> String
    fromSimpleTerm (STerm w) = w
    fromSimpleTerm (NotTerm w) = w
  divideQuery = partition isSTerm where
    isSTerm :: SimpleTerm -> Bool
    isSTerm (STerm _) = True
    isSTerm (NotTerm _) = False

alternatives :: (Monoid a) => a -> [a] -> a
alternatives delim as = mconcat $ intersperse delim as

data Decorated = Decorated String | NotDecorated String
makeSnippet :: Doc -> Int -> [Word] -> [Decorated]
makeSnippet doc parPos ws = map decorate $ doc !! parPos
       where
       decorate :: String -> Decorated
       decorate w = if w `elem` ws
                    then Decorated w
                    else NotDecorated w

printDecorated :: Decorated -> IO ()
printDecorated (Decorated s) = do
    setSGR [SetConsoleIntensity BoldIntensity, SetUnderlining SingleUnderline, SetColor Background Dull Green]
    putStr s
    setSGR [Reset]
printDecorated (NotDecorated s) = putStr s

printSnippet :: Doc -> Int -> [Word] -> IO ()
printSnippet d p ws = do
    mapM_ (\ x -> printDecorated x >> putStr " ") $ makeSnippet d p $ filteredWords
    putStrLn ""
    where
        filteredWords = odds $ filterQuery ws

odds :: [a] -> [a]
odds [] = []
odds [x] = [x]
odds (x : _ : xs) = x : odds xs
filterQuery :: [String] -> [String]
filterQuery = filter (not . flip elem nots)

printHelp :: IO ()
printHelp = do
    putTabStrLn $ "Exit commands : " ++ alternatives " or " (makeCommands exitCommands)
    putTabStrLn $ "Commands to operate results : " ++ alternatives " " (makeCommands prevCommands)
    putTabStrLn $ "Logical `NOT` : " ++ alternatives ", " nots
    putTabStrLn $ "Logical `AND` : " ++ alternatives ", " ands
    putTabStrLn $ "Logical `OR` : " ++ alternatives ", " ors
    return ()
putTabStrLn :: String -> IO ()
putTabStrLn s = do
    putStr "\t"
    putStrLn s

printError :: String -> IO ()
printError e = do
    setSGR [SetConsoleIntensity BoldIntensity, SetColor Background Vivid Red ]
    putStr "Error : "
    putStr e
    setSGR [Reset]
    putStrLn ""
makeCommands :: [String] -> [String]
makeCommands = map (":" ++)
exitCommands :: [String]
exitCommands = ["quit", "q"]
prevCommands :: [String]
next, previous :: String
previous = "p"
next = "n"
prevCommands = [next, previous]

data Command = Exit | Previous | Next
command :: String -> Maybe Command
command s
    | s `elem` exitCommands = Just Exit
    | s == previous = Just Previous
    | s == next = Just Next
    | otherwise = Nothing

runCommand :: Maybe ResultType -> Command -> IO Int
runCommand (Just([], _, _)) _ = do
    printError "Empty results"
    return 0
runCommand (Just(res, currentPos, _)) c =
    case c of
        Previous -> runPrevious
        Next -> runNext
    where
        size :: Int
        size = length res
        runPrevious = return $ max 0 (currentPos - 1)
        runNext     = return $ min (size - 1) (currentPos + 1)

type ResultType = ([Int], Int, String)
loop :: IIndex -> Maybe ResultType -> IO ()
loop i@(IIndex d _) = loop'
    where
    loop' prev = do
        printHelp
        case prev of
            Nothing -> return ()
            Just (res, pos, q) -> do
                putStrLn $ "Query : " ++ q
                if null res
                then printError "No result"
                else do 
                    putStrLn $ "Result (" ++ (show $ pos + 1) ++ "/" ++ (show $ length res) ++ ") : "
                      ++ "\nparagraph #" ++ show num 
                    printSnippet d num (words q)
                    where num = (res !! pos)
        putStr "Query or command > "
        hFlush stdout
        qc <- getLine
        if (not $ null qc)
        then
            case (head qc) of
            ':' ->
                case (command $ tail qc) of
                Just Exit -> do
                    exitSuccess
                Just c ->
                    case prev of
                    Nothing -> printError "no previous results"
                    Just just@(res, currentPos, q) -> do 
                        newPos <- runCommand (Just just) c
                        when (currentPos == newPos) $ printError "No more results"
                        loop' $ Just(res, newPos, q)
                Nothing -> printError $ "Not a command : " ++ qc
            _   ->
                case makeSimpleQuery qc of
                Nothing -> printError "Wrong Format"
                Just query -> do
                    loop' $ Just (result, 0, qc)
                    where result = S.toList $ runSimpleQuery i query
        else printError "Empty string"
        loop' prev
