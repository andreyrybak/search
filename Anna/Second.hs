import System.Environment
import Anna.Interpreter
import IIndex
import System.IO


main :: IO ()
main = do
    args <- getArgs 
    let filename = head args
    inputHandle <- openFile filename ReadMode 
    hSetEncoding inputHandle utf8
    input <- hGetContents inputHandle
    loop (buildIIndex (map words $ lines input)) Nothing
