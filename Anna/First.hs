import qualified System.IO.UTF8 as U
import System.Environment
import qualified Data.Map as M
import Data.List

import IIndex

printStatistics :: IIndex -> IO ()
printStatistics (IIndex doc m) = do
    putStrLn $ "Number of tokens : " ++ show tokenCount
    putStrLn $ "Number of terms : " ++ show termCount
    putStrLn $ "Average token length : " ++ show tokenAverageLength
    putStrLn $ "Average term length : " ++ show termAverageLength
    return ()
    where
    tokenCount :: Integer
    tokenCount = sum $ map (toInteger . length) doc
    tokenAverageLength :: Double
    tokenAverageLength = fromInteger (sum $ map (sum . map (toInteger . length)) doc) / fromInteger tokenCount
    termCount = M.size m
    termAverageLength :: Double
    termAverageLength = averageLength $ M.keys m
    averageLength :: [[a]] -> Double
    averageLength xs = ((sum . map genericLength) xs) / (genericLength xs)

main :: IO ()
main = do
    args <- getArgs 
    input <- U.readFile $ head args
    printStatistics $ buildIIndex (map words $ lines input)
