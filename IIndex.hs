module IIndex
       (IIndex(..), buildIIndex,
        lookUp, lookUpAll, lookUpAny,
        Doc, Word)
       where
import Prelude hiding (Word)
import qualified Data.Map as M
import qualified Data.IntSet as S


type Word = String
type Paragraph = [Word]
type Doc = [Paragraph]

data IIndex = IIndex Doc (M.Map Word S.IntSet) deriving Show

buildIIndex :: Doc -> IIndex 
buildIIndex text = IIndex text m
                   where
        enum = zip [0..] text
        m = foldl f M.empty enum
        f m' (i, p) = foldl g m' p
            where g m'' w = M.insertWith S.union w (S.singleton i) m''
     
lookUp :: IIndex -> Word -> S.IntSet
lookUp (IIndex _ m) w = M.findWithDefault S.empty w m

lookUpAll :: IIndex -> [Word] -> S.IntSet
lookUpAll i ws = foldl1 S.intersection (map (lookUp i) ws)

lookUpAny :: IIndex -> [Word] -> S.IntSet
lookUpAny _ [] = S.empty
lookUpAny i ws = foldl1 S.union (map (lookUp i) ws)


-- buildII :: [FilePath] -> IO IIndex
-- buildII files =
--     liftM (IIndex files . foldl f M.empty . zip [0..]) $
--     mapM readFile files
--   where f m (i, s) =
--             foldl g m $ map (lowercase . filter isAlpha) $ words s
--           where g m word = M.insertWith S.union word (S.singleton i) m
